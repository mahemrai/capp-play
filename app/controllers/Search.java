package controllers;

import java.util.ArrayList;
import java.util.List;

import models.ApiClient.Lastfm;
import models.Struct.Album;
import models.Struct.Artist;

import play.*;
import play.mvc.*;
import play.data.DynamicForm;
import play.data.Form;

import views.html.*;
import views.html.search.artists;
import views.html.search.albums;
import views.html.album.info;

/**
 * Search controller class
 * @author mahendra
 */

public class Search extends Controller {
	
	/**
	 * Handle search request for an artist and display the result.
	 * @return
	 * @throws Exception
	 */
	public static Result artistSearch() throws Exception {
		DynamicForm requestData = Form.form().bindFromRequest();
		String artist = requestData.get("artist");
		
		if(!artist.isEmpty()) {
			Lastfm lastfm = new Lastfm();
			List<Artist> list = lastfm.getArtists(artist);
			
			return ok(artists.render(list));
		}
		else {
			flash("error","Please enter name of the artist.");
			return redirect("/");
		}
	}
	
	/**
	 * Handle search request for albums of the selected artist and display 
	 * the result.
	 * @param artist
	 * @return
	 * @throws Exception
	 */
	public static Result albumSearch(String artist) throws Exception {
		DynamicForm requestData = Form.form().bindFromRequest();
		String artist_id = requestData.get("mbid");
		
		Lastfm lastfm = new Lastfm();
		List<Album> list = lastfm.getAlbums(artist_id);
		
		return ok(albums.render(list, artist_id, artist));
	}
	
	/**
	 * Display an album information after retrieving it from Last.fm.
	 * @param artist
	 * @param album
	 * @return
	 * @throws Exception
	 */
	public static Result albumInfo(String artist, String album) throws Exception {
		DynamicForm requestData = Form.form().bindFromRequest();
		String mbid = requestData.get("mbid");
		
		Lastfm lastfm = new Lastfm();
		Album data = lastfm.getAlbumInfo(mbid);
		
		return ok(info.render(data, artist));
	}
}
