package controllers;

import models.DbModel.Albums;
import models.DbModel.Artists;
import play.*;
import play.mvc.*;

import views.html.*;
import views.html.site.artistlist;
import views.html.site.albumlist;
import views.html.site.index;

public class Site extends Controller {
    public static Result index() {
    	return ok(
    		index.render(
    			Artists.fetchRecent(),
    			Albums.fetchRecent()
    	));
    }
    
    public static Result artists(int page) {
    	return ok(
    		artistlist.render(
    			Artists.page(page-1, 10)
    		)
    	);
    }
    
    public static Result albums(int page) {
    	return ok(
    		albumlist.render(
    			Albums.page(page-1, 10)
    		)
    	);
    }
}
