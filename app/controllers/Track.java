package controllers;

import java.util.List;
import java.io.IOException;

import org.json.JSONException;

import com.fasterxml.jackson.databind.node.ObjectNode;

import models.ApiClient.YoutubeClient;
import play.libs.Json;
import play.mvc.*;

public class Track extends Controller {
	public static Result showVideo(String query) throws IOException, JSONException {
		ObjectNode result = Json.newObject();
		YoutubeClient client = new YoutubeClient();
		String videoId = client.fetchVideo(query);
		result.put("status", "OK");
		result.put("id", videoId);
		return ok(result);
	}
}
