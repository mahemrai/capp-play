package controllers;

import java.util.List;

import play.*;
import play.mvc.*;
import play.data.DynamicForm;
import play.data.Form;

import models.ApiClient.Echonest;
import models.DbModel.Albums;
import models.DbModel.Artists;
import models.Struct.News;

import views.html.*;
import views.html.artist.info;

public class Artist extends Controller {
    public static Result addArtist() {
    	Artists artist = new Artists();
        DynamicForm requestData = Form.form().bindFromRequest();
        
        artist.create(requestData.get("artist"), requestData.get("mbid"), requestData.get("image"));
        
        String artist_name = requestData.get("artist").replaceAll("\\+", " ");
        
        flash("success", "Artist: " + artist_name + " successfully added to the list.");
    	return redirect("/");
    }
    
    public static Result showArtistInfo(String mbid) throws Exception {
    	Echonest echonest = new Echonest();
    	List<News> news = echonest.getArtistNews(mbid);
    	List<models.Struct.Artist> similar_artists = echonest.getSimilarArtists(mbid);
    	
    	return ok(
    		info.render(
    			Artists.fetchOne(mbid),
    			Albums.fetchByArtist(mbid),
    			news, similar_artists
    		)
    	);
    }
}
