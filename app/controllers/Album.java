package controllers;

import java.util.ArrayList;
import java.util.List;

import play.*;
import play.mvc.*;
import play.data.DynamicForm;
import play.data.Form;

import models.ApiClient.Lastfm;
import models.DbModel.Albums;
import models.DbModel.Artists;
import models.DbModel.Tracks;

import views.html.*;
import views.html.album.info;

public class Album extends Controller {
	/**
	 * Handle request for adding new album to the database.
	 * @return
	 * @throws Exception
	 */
    public static Result addAlbum() throws Exception {
    	Albums album = new Albums();
    	Lastfm lastfm = new Lastfm();
    	
    	DynamicForm requestData = Form.form().bindFromRequest();
    	models.Struct.Album data = lastfm.getAlbumInfo(requestData.get("mbid"));
    	
    	album.create(requestData.get("album"), requestData.get("artist_id"), requestData.get("mbid"), requestData.get("image"));
    	
    	Tracks track = new Tracks();
    	track.create(requestData.get("mbid"), data.getTracks());
    	
    	flash("success", "Album: " + requestData.get("album").replaceAll("\\+", " ") + " successfully added to the list.");
    	return redirect("/");
    }
    
    /**
     * Load information page for the selected album.
     * @param album_id
     * @return
     * @throws Exception
     */
    public static Result showAlbumInfo(String album_id) throws Exception {
    	models.Struct.Album album = new models.Struct.Album();
    	
    	album.setArtistId(Albums.fetchOne(album_id).artist_id);
    	album.setImage(Albums.fetchOne(album_id).image);
    	album.setMbid(Albums.fetchOne(album_id).mbid);
    	album.setName(Albums.fetchOne(album_id).name);
    	
    	Artists artist = Artists.fetchOne(Albums.fetchOne(album_id).artist_id);
    	
    	ArrayList<models.Struct.Track> album_tracks = new ArrayList<models.Struct.Track>();
    	
    	List<Tracks> list = Tracks.fetchByAlbum(album_id);
    	
    	for(Tracks item : list) {
    		models.Struct.Track track = new models.Struct.Track();
    		track.setName(item.name);
    		track.setDuration(item.duration);
    		album_tracks.add(track);
    	}
    	
    	album.setTracks(album_tracks);
    	
    	return ok(
    		info.render(
    			album,
    			artist.name
    		)
    	);
    }
}
