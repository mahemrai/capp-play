package models.ApiClient;

import java.io.*;
import java.net.*;

public class ApiClient {
    protected String api_url;
    
    /**
     * Send HTTP request to the provided url and retrieve response as a string.
     * @param api_url
     * @return
     * @throws Exception
     */
    protected String sendRequest(String api_url) throws Exception {
    	URL service_url = new URL(api_url);
    	URLConnection service_connection = service_url.openConnection();
    	
    	BufferedReader reader = new BufferedReader(
    		new InputStreamReader(service_connection.getInputStream())
    	);
    	    	
    	String response = reader.readLine();
    	
    	reader.close();
    	
    	return response;
    }
}
