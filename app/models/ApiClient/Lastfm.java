package models.ApiClient;

import java.util.ArrayList;
import java.util.List;

import models.Struct.Album;
import models.Struct.Artist;
import models.Struct.Track;

import org.json.*;

/**
 * Lastfm class
 * @author mahendra
 */

public class Lastfm extends ApiClient {
    private final String API_KEY = "2dc8fe6d2ff0548b989539ff8f03c8be";
    
    protected String api_url;
    
    /**
     * Constructor
     */
    public Lastfm() {
    	this.api_url = "http://ws.audioscrobbler.com/2.0/";
    }
    
    /**
     * Perform search for artist by sending HTTP request to Last.fm API and 
     * return list of artists by extracting relevant data from the JSON response.
     * @param artist_name
     * @return
     * @throws Exception
     */
    public List<Artist> getArtists(String artist_name) throws Exception {
    	ArrayList<Artist> list = new ArrayList<Artist>();
    	
    	String url = this.api_url + "?method=artist.search&artist=" + 
                     artist_name.replace(' ', '+') + "&api_key=" + this.API_KEY + "&format=json";
    	
    	String json = this.sendRequest(url);
    	
    	JSONObject response = new JSONObject(json);
    	JSONObject data = response.getJSONObject("results");
    	JSONObject artistmatches = data.getJSONObject("artistmatches");
    	
    	JSONArray artists = artistmatches.getJSONArray("artist");
    	
    	for(int i=0; i<artists.length(); i++) {
    		JSONObject item = artists.getJSONObject(i);
    		
    		Artist artist = new Artist();
    		artist.setName(item.getString("name"));
    		artist.setMbid(item.getString("mbid"));
    		artist.setImage(this.getImage(item));
    		
    		list.add(artist);
    	}
    	
    	return list;
    }
    
    /**
     * Perform search for selected artist's albums by sending HTTP request to 
     * Last.fm API and return list of albums by extracting relevant data from 
     * the JSON response.
     * @param mbid
     * @return
     * @throws Exception
     */
    public List<Album> getAlbums(String mbid) throws Exception {
    	ArrayList<Album> list = new ArrayList<Album>();
    	
    	String url = this.api_url + "?method=artist.gettopalbums&mbid=" + mbid + 
    			     "&api_key=" + this.API_KEY + "&format=json";
    	
    	String json = this.sendRequest(url);
    	
    	JSONObject response = new JSONObject(json);
    	JSONObject data = response.getJSONObject("topalbums");
    	
    	JSONArray albums = data.getJSONArray("album");
    	
    	for(int i=0; i<albums.length(); i++) {
    		JSONObject item = albums.getJSONObject(i);
    		
    		Album album = new Album();
    		album.setName(item.getString("name"));
    		album.setMbid(item.getString("mbid"));
    		album.setImage(this.getImage(item));
    		
    		list.add(album);
    	}
    	
    	return list;
    }
    
    /**
     * Retrieve information for the selected album by sending HTTP request to 
     * Last.fm API and extracting required data from the JSON response.
     * @param mbid
     * @return
     * @throws Exception
     */
    public Album getAlbumInfo(String mbid) throws Exception {
    	String url = this.api_url + "?method=album.getInfo&api_key=" + this.API_KEY + 
    			     "&mbid=" + mbid + "&format=json";
    	
    	String json = this.sendRequest(url);
    	
    	JSONObject response = new JSONObject(json);
    	JSONObject data = response.getJSONObject("album");
    	
    	Album album = new Album();
    	album.setName(data.getString("name"));
    	album.setMbid(data.getString("mbid"));
    	album.setImage(this.getImage(data));
    	album.setTracks(this.getTracks(data));
    	
    	return album;
    }
    
    /**
     * Extract large image from the passed JSONObject.
     * @param artist
     * @return
     * @throws Exception
     */
    protected String getImage(JSONObject artist) throws Exception {
    	JSONArray images = artist.getJSONArray("image");
    	JSONObject large_image = images.getJSONObject(2);
    	return large_image.getString("#text");
    }
    
    /**
     * Extract track information from the provided JSONObject and return the list.
     * @param data
     * @return
     * @throws Exception
     */
    protected ArrayList<Track> getTracks(JSONObject data) throws Exception {
    	ArrayList<Track> list = new ArrayList<Track>();
    	
    	JSONObject track_obj = data.getJSONObject("tracks");
    	
    	JSONArray tracks = track_obj.getJSONArray("track");
    	
    	for(int i=0; i<tracks.length(); i++) {
    		JSONObject track = tracks.getJSONObject(i);
    		
    		Track item = new Track();
    		item.setName(track.getString("name"));
    		item.setDuration(track.getInt("duration"));
    		
    		list.add(item);
    	}
    	return list;
    }
}
