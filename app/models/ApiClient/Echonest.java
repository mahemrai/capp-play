package models.ApiClient;

import java.util.ArrayList;
import java.util.List;

import models.Struct.Artist;
import models.Struct.News;

import play.libs.WS;
import play.mvc.Result;

import static play.libs.F.Function;
import static play.libs.F.Promise;

import org.json.*;

public class Echonest extends ApiClient{
    private final String API_KEY = "AHAVGTJRT7B3O76IY";
    
    protected String api_url;
    
    /**
     * Constructor
     */
    public Echonest() {
    	this.api_url = "http://developer.echonest.com/api/v4/";
    }
    
    public List<News> getArtistNews(String artist_id) throws Exception {
    	ArrayList<News> list = new ArrayList<News>();
    	
    	String url = this.api_url + "artist/news?api_key=" + this.API_KEY + "&id=musicbrainz:artist:" + artist_id;
    	
    	String json = this.sendRequest(url);
    	
    	JSONObject response = new JSONObject(json);
    	JSONObject data = response.getJSONObject("response");
    	
    	if (data.has("news")) {
	    	JSONArray news = data.getJSONArray("news");
	    	
	    	for(int i=0; i<news.length(); i++) {
	    		JSONObject item = news.getJSONObject(i);
	    		
	    		News news_item = new News();
	    		news_item.setName(item.getString("name"));
	    		news_item.setUrl(item.getString("url"));
	    		
	    		list.add(news_item);
	    	}
    	}
    	
    	return list;
    }
    
    public List<Artist> getSimilarArtists(String artist_id) throws Exception {
    	ArrayList<Artist> list = new ArrayList<Artist>();
    	
    	String url = this.api_url + "artist/similar?api_key=" + this.API_KEY + "&id=musicbrainz:artist:" + artist_id + "&bucket=id:musicbrainz";
    	
    	String json = this.sendRequest(url);
    	
    	JSONObject response = new JSONObject(json);
    	JSONObject data = response.getJSONObject("response");
    	
    	if (data.has("artists")) {
	    	JSONArray artists = data.getJSONArray("artists");
	    	
	    	for (int i=0; i<artists.length(); i++) {
	    		JSONObject item = artists.getJSONObject(i);
	    		
	    		Artist artist_item = new Artist();
	    		artist_item.setName(item.getString("name"));
	    		
	    		if (item.has("foreign_ids")) {
		    		JSONArray foreign_ids = item.getJSONArray("foreign_ids");
		    		JSONObject foreign_id = foreign_ids.getJSONObject(0);
	    		    artist_item.setMbid(foreign_id.getString("foreign_id").split("musicbrainz:artist:")[1]);
	    		}
	    		
	    		list.add(artist_item);
	    	}
    	}
    	
    	return list;
    }
}
