package models.ApiClient;

import java.io.IOException;
import java.util.List;

import models.Struct.YoutubeVideo;

import org.json.*;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

public class YoutubeClient {
	private static final String DEVELOPER_KEY = "AIzaSyAGtRA1esIKKOGjERpS4X7BvPsLHoFL7B4";
	private static final long MAX_RESULTS = 1;
	
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();
	
	private YouTube youtube;
	
	public String fetchVideo(String query) throws IOException, JSONException {
		this.youtube = new YouTube.Builder(this.HTTP_TRANSPORT, this.JSON_FACTORY, new HttpRequestInitializer() {
			public void initialize(HttpRequest request) throws IOException {
			}
		}).build();
		
		YouTube.Search.List search = youtube.search().list("id");
		
		search.setKey(this.DEVELOPER_KEY);
		search.setQ(query);
		search.setType("video");
		search.setMaxResults(this.MAX_RESULTS);
		
		SearchListResponse searchResponse = search.execute();
		
		JSONObject response = new JSONObject(searchResponse);
		JSONArray item = response.getJSONArray("items");
		JSONObject video = item.getJSONObject(0);
		JSONObject id = video.getJSONObject("id");
		
		return id.getString("videoId");
	}
}
