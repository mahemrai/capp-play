package models.Struct;

import java.util.ArrayList;
import java.util.List;

/**
 * Album class
 * @author mahendra
 */

public class Album {
	protected String artist_id;
    protected String name;
    protected String mbid;
    protected String image;
    protected ArrayList<Track> tracks;
    
    public void setArtistId(String artist_id) {
    	this.artist_id = artist_id;
    }
    
    public void setName(String name) {
    	this.name = name;
    }
    
    public void setMbid(String mbid) {
    	this.mbid = mbid;
    }
    
    public void setImage(String image) {
    	this.image = image;
    }
    
    public void setTracks(ArrayList<Track> tracks) {
    	this.tracks = tracks;
    }
    
    public String getArtistId() {
    	return this.artist_id;
    }
    
    public String getName() {
    	return this.name;
    }
    
    public String getMbid() {
    	return this.mbid;
    }
    
    public String getImage() {
    	return this.image;
    }
    
    public List<Track> getTracks() {
    	return this.tracks;
    }
}
