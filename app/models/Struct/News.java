package models.Struct;

public class News {
    protected String name;
    protected String url;
    
    public void setName(String name) {
    	this.name = name;
    }
    
    public void setUrl(String url) {
    	this.url = url;
    }
    
    public String getName() {
    	return this.name;
    }
    
    public String getUrl() {
    	return this.url;
    }
}
