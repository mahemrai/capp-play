package models.Struct;

/**
 * Track class
 * @author mahendra
 */

public class Track {
    protected String name;
    protected int duration;
    
    public void setName(String name) {
    	this.name = name;
    }
    
    public void setDuration(int duration) {
    	this.duration = duration;
    }
    
    public String getName() {
    	return this.name;
    }
    
    public int getDuration() {
    	return this.duration;
    }
}
