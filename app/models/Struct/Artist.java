package models.Struct;

public class Artist {
    protected String name;
    protected String mbid;
    protected String image;
    
    public void setName(String name) {
    	this.name = name;
    }
    
    public void setMbid(String mbid) {
    	this.mbid = mbid;
    }
    
    public void setImage(String image) {
    	this.image = image;
    }
    
    public String getName() { 
    	return this.name;
    }
    
    public String getMbid() { 
    	return this.mbid; 
    }
    
    public String getImage() { 
    	return this.image; 
    }
}
