package models.DbModel;

import java.util.*;

import javax.persistence.*;

import models.Struct.Track;

import play.db.ebean.*;
import play.db.ebean.Model.Finder;
import play.data.validation.Constraints.*;

@Entity
public class Tracks extends Model {
    @Id
    public Long id;
    @Required
    public String album_id;
    @Required
    public String name;
    public Integer duration;
    public Date created_at;
    public Date updated_at;
    
    public static Finder<String, Tracks> find = new Finder(
    	Long.class, Tracks.class
    );    
    
    public static List<Tracks> all() {
    	return find.all();
    }
    
    public static List<Tracks> fetchByAlbum(String album_id) {
    	return find.where().eq("album_id", album_id.toLowerCase()).findList();
    }
    
    public void create(String album_mbid, List<Track> tracks) {
    	for(Track track : tracks) {
    		Tracks row = new Tracks();
    		row.album_id = album_mbid;
    		row.name = track.getName();
    		row.duration = track.getDuration();
    		row.created_at = Calendar.getInstance().getTime();
    		row.updated_at = Calendar.getInstance().getTime();
    		
    		row.save();
    	}
    }
}
