package models.DbModel;

import java.util.*;

import play.db.ebean.*;
import play.db.ebean.Model.Finder;
import play.data.validation.Constraints.*;

import javax.persistence.*;

import com.avaje.ebean.*;

@Entity
public class Artists extends Model {
	@Id
    public Long id;
	@Required
    public String name;
	@Required
    public String mbid;
    public String image;
    public Date created_at;
    public Date updated_at;
    
    public static Finder<String, Artists> find = new Finder(
    	String.class, Artists.class
    );
    
    public static List<Artists> all() {
    	return find.all();
    }
    
    public static Artists fetchOne(String mbid) {
    	return find.where().eq("mbid", mbid.toLowerCase()).findUnique();
    }
    
    public static Page<Artists> fetchRecent() {
    	return 
    	    find.orderBy("id desc").findPagingList(4).getPage(0);
    }
    
    public static Page<Artists> page(int page, int pageSize) {
    	return
    		find.where()
    		    .orderBy("created_at DESC")
    		    .findPagingList(pageSize)
    		    .setFetchAhead(false)
    		    .getPage(page);
    }
    
    public void create(String name, String mbid, String image) {
    	this.name = name.replace('+', ' ');
    	this.mbid = mbid;
    	this.image = image;
    	this.created_at = Calendar.getInstance().getTime();
    	this.updated_at = Calendar.getInstance().getTime();
    	
    	this.save();
    }
}
