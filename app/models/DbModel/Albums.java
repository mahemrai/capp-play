package models.DbModel;

import java.util.*;

import play.db.ebean.*;
import play.db.ebean.Model.Finder;
import play.data.validation.Constraints.*;

import javax.persistence.*;

import com.avaje.ebean.*;

@Entity
public class Albums extends Model {
    @Id
    public Long id;
    @Required
    public String name;
    @Required
    public String artist_id;
    @Required
    public String mbid;
    public String image;
    public Date created_at;
    public Date updated_at;
    
    public static Finder<String, Albums> find = new Finder(
    	Long.class, Albums.class
    );
    
    public static List<Albums> all() {
    	return find.all();
    }
    
    public static Albums fetchOne(String album_id) {
    	return find.where().eq("mbid", album_id.toLowerCase()).findUnique();
    }
    
    public static List<Albums> fetchByArtist(String artist_id) {
    	return find.where().eq("artist_id", artist_id.toLowerCase()).findList();
    }
    
    public static Page<Albums> fetchRecent() {
    	return find.orderBy("id desc").findPagingList(4).getPage(0);
    }
    
    public static Page<Albums> page(int page, int pageSize) {
    	return
    		find.where()
    		    .orderBy("created_at DESC")
    		    .findPagingList(pageSize)
    		    .setFetchAhead(false)
    		    .getPage(page);
    }
    
    public void create(String name, String artist_id, String mbid, String image) {
    	this.name = name.replace('+', ' ');
    	this.artist_id = artist_id;
    	this.mbid = mbid;
    	this.image = image;
    	this.created_at = Calendar.getInstance().getTime();
    	this.updated_at = Calendar.getInstance().getTime();
    	
    	this.save();
    }
}
