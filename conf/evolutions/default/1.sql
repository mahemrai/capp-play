# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table albums (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  artist_id                 varchar(255),
  mbid                      varchar(255),
  image                     varchar(255),
  created_at                datetime,
  updated_at                datetime,
  constraint pk_albums primary key (id))
;

create table artists (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  mbid                      varchar(255),
  image                     varchar(255),
  created_at                datetime,
  updated_at                datetime,
  constraint pk_artists primary key (id))
;

create table tracks (
  id                        bigint auto_increment not null,
  album_id                  varchar(255),
  name                      varchar(255),
  duration                  integer,
  created_at                datetime,
  updated_at                datetime,
  constraint pk_tracks primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table albums;

drop table artists;

drop table tracks;

SET FOREIGN_KEY_CHECKS=1;

