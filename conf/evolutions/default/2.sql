# --- !Ups

create table albums (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  artist_id                 varchar(255),
  mbid                      varchar(255),
  image                     varchar(255),
  created_at                datetime,
  updated_at                datetime,
  constraint pk_albums primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table albums;

SET FOREIGN_KEY_CHECKS=1;
