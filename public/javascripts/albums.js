$(document).ready(function() {
	$('.video-track').click(function(e) {
		e.preventDefault();
		track = $(this).attr('data');
		
		$.getJSON('/video/'+track, function(data) {
			var embedded_video = '<button type="button" class="close" data-dismiss="modal">&times;</button>' + 
				'<iframe id=ytplayer type=text/html width=100% height=390 '+
		        'src=http://www.youtube.com/embed/'+data.id+'?autoplay=1 frameborder=0 />';
			
			$('.modal-body').html(embedded_video);
		});
	})
});